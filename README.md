# [symfony/twig-pack](https://packagist.org/packages/symfony/twig-pack)

* Symfony5: The Fast Track
  * [*Step 10: Building the User Interface*
    ](https://symfony.com/doc/current/the-fast-track/en/10-twig.html)
  * [*Step 12: Listening to Events*
    ](https://symfony.com/doc/current/the-fast-track/en/12-event.html)
